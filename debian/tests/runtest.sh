#!/bin/sh

set -e

caldera &

sleep 60s

if ! curl 127.0.0.1:8888 | grep CALDERA; then
    echo "FAILURE: Web UI test failed"
    exit 1
else
    echo "Login page is OK"
fi

PASSWORD=$(yq -r '.users.red.red' /usr/share/caldera/conf/local.yml)

echo $PASSWORD

curl -d "username=red" -d "password=$PASSWORD" -c cookiefile 127.0.0.1:8888/enter
if ! curl -b cookiefile 127.0.0.1:8888 | grep "Red Team | CALDERA"; then
    echo "FAILURE: Login for user red failed"
    exit 1
else
    echo "Login for user red is successful"
fi

